# SevenGameHelper

#### Description
奇游自动化测试框架
    奇游智能化测试框架至力打造H5、手机APP、手游、FLASH游戏的纯自动化测试框。软件基于易语言、DM图色漠块和ADB终端命令行开发。集成了各大操作模块于一身，并免费提供给开发者做二次开发。
    本软件仅作为软件测试用，请勿用于非法用途，本软件免费。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
