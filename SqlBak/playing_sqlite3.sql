/*
Navicat SQLite Data Transfer

Source Server         : SQLite
Source Server Version : 30714
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30714
File Encoding         : 65001

Date: 2020-04-20 12:06:01
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for pc_game_playing
-- ----------------------------
DROP TABLE IF EXISTS "main"."pc_game_playing";
CREATE TABLE "pc_game_playing" (
"id"  INTEGER NOT NULL,
"ptName"  TEXT(30),
"gameID"  TEXT(30),
"gameName"  TEXT(30),
"opName"  TEXT(30),
"serverName"  TEXT(30),
"gameLink"  TEXT(255),
"gameUser"  TEXT(30),
"gamePassword"  TEXT(30),
"isYouxiao"  INTEGER,
"opTime"  INTEGER,
"isComplete"  INTEGER,
"gameType"  TEXT(30),
"roleLevel"  INTEGER,
"imei"  TEXT(50),
"rolePower"  INTEGER,
"awakenLevel"  INTEGER,
"isNew"  INTEGER,
"LoginIP"  TEXT(30),
PRIMARY KEY ("id" ASC)
);

-- ----------------------------
-- Indexes structure for table pc_game_playing
-- ----------------------------
CREATE UNIQUE INDEX "main"."ptNameGameID"
ON "pc_game_playing" ("ptName" ASC, "gameID" ASC);
